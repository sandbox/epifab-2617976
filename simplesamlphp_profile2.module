<?php
/**
 * @file
 * Integrates profile2 and simplesamlphp_auth modules.
 * Exposes a way to map profile fields to SimpleSAML attributes.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function simplesamlphp_profile2_form_simplesamlphp_auth_settings_sync_alter(&$form, &$form_state) {
  // Loads profile types
  $profile_types = profile2_get_types();
  // Sorts profiles by weight
  usort($profile_types, function($a, $b) { return $a->weight - $b->weight; });

  // Gets configuration default
  $profiles_default = variable_get('simplesamlphp_profile2', array());

  $form['simplesamlphp_profile2'] = array('#tree' => TRUE);

  foreach ($profile_types as $profile_type) {
    $fields_default = isset($profiles_default[$profile_type->type])
      ? $profiles_default[$profile_type->type]
      : array();

    $form['simplesamlphp_profile2'][$profile_type->type] = array(
      '#type' => 'fieldset',
      '#title' => t('!profile profile', array('!profile' => $profile_type->label)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $profile_fields = field_info_instances('profile2', $profile_type->type);

    foreach ($profile_fields as $profile_field) {
      $form['simplesamlphp_profile2'][$profile_type->type][$profile_field['field_name']] = array(
        '#type' => 'textfield',
        '#title' => $profile_field['label'],
        '#default_value' => isset($fields_default[$profile_field['field_name']])
          ? $fields_default[$profile_field['field_name']]
          : NULL
      );
    }
  }
}

/**
 * Implements hook_simplesamlphp_auth_after_login().
 */
function simplesamlphp_profile2_simplesamlphp_auth_after_login($attributes, $account) {
  $profiles_mapping = variable_get('simplesamlphp_profile2', array());

  foreach ($profiles_mapping as $profile_type => $fields_mapping) {
    $profile_values = array();

    foreach ($fields_mapping as $field_name => $attribute_name) {
      if (empty($attribute_name)) {
        continue;
      }
      elseif (!isset($attributes[$attribute_name])) {
        watchdog('simplesamlphp_profile2', 'IdP attribute %attribute is missing.',
          array('%attribute' => $attribute_name), WATCHDOG_WARNING);
      }
      else {
        $profile_values[$field_name] = $attributes[$attribute_name];
      }
    }

    if (!empty($profile_values)) {
      _simplesamlphp_profile2_save_profile($account, $profile_type, $profile_values);
    }
  }
}

/**
 * Creates or updates a user profile.
 *
 * @param object $account User account object
 * @param string $profile_type Profile type
 * @param array $profile_values Profile values
 */
function _simplesamlphp_profile2_save_profile($account, $profile_type, $profile_values) {
  $profile = profile2_load_by_user($account, $profile_type);

  if (empty($profile)) {
    $profile = profile2_create(array(
      'type' => $profile_type,
      'uid' => $account->uid
    ));
  }

  foreach ($profile_values as $field_name => $values) {
    if (empty($values)) {
      unset($profile->{$field_name});
    }
    else {
      // Handles fields with multiple values
      foreach ((array)$values as $value) {
        $profile->{$field_name}[LANGUAGE_NONE][] = array('value' => $value);
      }
    }
  }

  profile2_save($profile);
}
